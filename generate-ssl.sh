#!/bin/bash

set -e

CURRENT_DATE=$(date +"%Y-%m-%d-%H-%M-%S")
CERT_FILE=${CURRENT_DATE}/fullchain.cer
KEY_FILE=${CURRENT_DATE}/fsci.in.key

echo "Generating Certificates"
/root/.acme.sh/acme.sh --issue --dns dns_dgon -d "fsci.in" -d "*.fsci.in" -d "fsci.org.in" -d "*.fsci.org.in" -d "fsug.in" -d "*.fsug.in" --yes-I-know-dns-manual-mode-enough-go-ahead-please --server letsencrypt

echo "Moving certificates to dated folder ${CURRENT_DATE}"
mkdir "${CURRENT_DATE}"
cp -vr ~/.acme.sh/fsci.in/* "${CURRENT_DATE}"

echo "Updating GitLab Pages certificates - fsci.in"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/8388067/pages/domains/fsci.in" > /dev/null

sleep 10

echo "Updating GitLab Pages certificates - fsci.org.in"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/8388067/pages/domains/fsci.org.in" > /dev/null
     
sleep 10

echo "Updating GitLab Pages certificates - fsug.in"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/2923175/pages/domains/fsug.in" > /dev/null

